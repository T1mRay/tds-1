// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDS_2_1_4GameMode.generated.h"

UCLASS(minimalapi)
class ATDS_2_1_4GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATDS_2_1_4GameMode();
};



