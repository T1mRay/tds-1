// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_2_1_4.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDS_2_1_4, "TDS_2_1_4" );

DEFINE_LOG_CATEGORY(LogTDS_2_1_4)
 